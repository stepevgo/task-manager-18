package ru.t1.stepanishchev.tm.command.user;

import ru.t1.stepanishchev.tm.api.service.IAuthService;
import ru.t1.stepanishchev.tm.api.service.IUserService;
import ru.t1.stepanishchev.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    protected IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

}