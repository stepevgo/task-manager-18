package ru.t1.stepanishchev.tm.command.user;

public final class UserLogoutCommand extends AbstractUserCommand {

    private final String NAME = "logout";

    private final String DESCRIPTION = "Logout current user.";

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}