package ru.t1.stepanishchev.tm.exception.user;

public final class ExistsEmailException extends AbstractUserException {

    public ExistsEmailException() {
        super("Error! Email alredy exists.");
    }

    public ExistsEmailException(String email) {
        super("Error! Email '" + email + "'alredy exists.");
    }

}