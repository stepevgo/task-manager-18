package ru.t1.stepanishchev.tm.service;

import ru.t1.stepanishchev.tm.api.repository.IUserRepository;
import ru.t1.stepanishchev.tm.api.service.IUserService;
import ru.t1.stepanishchev.tm.enumerated.Role;
import ru.t1.stepanishchev.tm.exception.entity.UserNotFoundException;
import ru.t1.stepanishchev.tm.exception.field.EmailEmptyException;
import ru.t1.stepanishchev.tm.exception.field.IdEmptyException;
import ru.t1.stepanishchev.tm.exception.field.LoginEmptyException;
import ru.t1.stepanishchev.tm.exception.field.PasswordEmptyException;
import ru.t1.stepanishchev.tm.exception.user.ExistsEmailException;
import ru.t1.stepanishchev.tm.exception.user.ExistsLoginException;
import ru.t1.stepanishchev.tm.exception.user.RoleEmptyException;
import ru.t1.stepanishchev.tm.model.User;
import ru.t1.stepanishchev.tm.util.HashUtil;

import java.util.List;

public final class UserService implements IUserService {

    private IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException(login);
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USUAL);
        return userRepository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException(login);
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException(login);
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        final User user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @Override
    public User add(final User user) {
        if (user == null) throw new UserNotFoundException();
        return userRepository.add(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        User user = userRepository.findById(id);
        if (user == null) return null;
        return user;
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        User user = userRepository.findByLogin(login);
        if (user == null) return null;
        return user;
    }

    @Override
    public User findByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new ExistsEmailException();
        User user = userRepository.findById(email);
        if (user == null) return null;
        return user;
    }

    @Override
    public User remove(final User user) {
        if (user == null) throw new UserNotFoundException();
        userRepository.remove(user);
        return user;
    }

    @Override
    public User removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = findById(id);
        return remove(user);
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        return remove(user);
    }

    @Override
    public User removeByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = findByEmail(email);
        return remove(user);
    }

    @Override
    public User setPassword(final String id, final String password) {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUser(
            final String id,
            final String firstName,
            final String lastName,
            final String middleName
    ) {
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public Boolean isLoginExist(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return userRepository.isLoginExist(login);
    }

    @Override
    public Boolean isEmailExist(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return userRepository.isEmailExist(email);
    }

}